'use strict'

//Прототипное наследование в js, это когда дочерний объект имеет доступ к свойствам и методам родительского
//теоретически их не перезаписывая, но это не точно

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    getName() {
        return this.name;
    };
    getAge() {
        return this.age;
    };
    getSalary() {
        return this.salary;
    };

    setName(newName) {
        this.name = newName;
    };
    setAge(newAge) {
        this.age = newAge;
    };
    setSalary(newSalary) {
        this.salary = newSalary;
    };
}


class Programmer extends Employee {
    constructor(name, age, salary, languages) {
        super(name, age, salary);
        this.langs = languages;
    };

    getSalary() {
        return super.getSalary() * 3;
    };
}

const firstDev = new Programmer('Ololosha', 11, 100500, ['ru', 'albanian']),
    secondDev = new Programmer('Pruzhinka', 20, 500, ['js', 'php']),
    thirdDev = new Programmer('Gandalf', 439875, 1, ['java', 'c++', 'js', 'python', 'scala', 'php', 'typescript', 'ruby'])

console.log(firstDev);
console.log(secondDev);
console.log(thirdDev);
